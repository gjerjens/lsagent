# @summary Installs the Lansweeper Agent Service (LsAgent)
#
# @example
#   include lsagent
class lsagent (
  Enum['present', 'absent'] $ensure,
  String $agentkey,
  String $server,
  Stdlib::HTTPSUrl $linux_download_url,
  Stdlib::Compat::Absolute_path $linux_download_path,
  Boolean $service_enable,
  Enum['running', 'stopped'] $service_ensure,
  Boolean $service_manage,
  String $service_name,
  Optional[String] $service_provider,
  Boolean $service_hasstatus,
  Boolean $service_hasrestart
) {

  if $facts['kernel'] == 'Linux' {

    archive { $linux_download_path :
      ensure  => $ensure,
      source  => $linux_download_url,
      extract => false,
      creates => '/opt/LansweeperAgent',
    }

    file { $linux_download_path :
      mode    => '0755',
      require => Archive[$linux_download_path],
    }

    exec { 'install':
      command     => "${linux_download_path} --mode unattended --server ${server} --agentkey ${agentkey}",
      subscribe   => Archive[$linux_download_path],
      refreshonly => true,
      require     => File[$linux_download_path],
    }

    if $service_manage == true {
      service { 'LansweeperAgentService':
        ensure     => $service_ensure,
        enable     => $service_enable,
        name       => $service_name,
        provider   => $service_provider,
        hasstatus  => $service_hasstatus,
        hasrestart => $service_hasrestart,
        require    => File[$linux_download_path],
      }
    }



  }

}
